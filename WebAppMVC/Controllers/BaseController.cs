﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAppMVC.BusinessLogic.Exceptions;

namespace WebAppMVC.Controllers
{
    public class BaseController: Controller
    {
        protected async Task<IActionResult> ExecuteAsync(Func<Task<IActionResult>> method, object model = null)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                IActionResult result = await method?.Invoke();
                return result;
            }
            catch (UnauthorizedException)
            {
                return RedirectToAction("SignIn", "Account", new { area = "AccountAdministration" });
            }
            catch (BadRequestException ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
            catch (InternalServerErrorException ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
        }
    }
}
