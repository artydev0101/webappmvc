﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAppMVC.BusinessLogic.Providers.Interfaces;
using WebAppMVC.Controllers;
using WebAppMVC.ViewModels.Responses.AuthorManagement;

namespace WebAppMVC.Areas.AuthorsManagement.Controllers
{
    [Area("AuthorManagement")]
    public class AuthorController: BaseController
    {
        private readonly IAuthorProvider _authorProvider;

        public AuthorController(IAuthorProvider authorProvider)
        {
            _authorProvider = authorProvider;
        }

        [HttpGet]
        public async Task<IActionResult> Get(long id)
        {
            return await ExecuteAsync(async () =>
            {
                GetAuthorAuthorManagementResponseViewModel getAuthorAuthorManagementResponseViewModel
                    = await _authorProvider.Get(id);

                return View(getAuthorAuthorManagementResponseViewModel);
            });
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return await ExecuteAsync(async () =>
            {
                GetAuthorListAuthorAuthorManagementResponseViewModel getAuthorListAuthorAuthorManagementResponseViewModel
                    = await _authorProvider.GetAuthorList();

                return View(getAuthorListAuthorAuthorManagementResponseViewModel);
            });
        }
    }
}