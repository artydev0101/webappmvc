﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebAppMVC.BusinessLogic.Services.Interfaces;
using WebAppMVC.Controllers;
using WebAppMVC.ViewModels.Requests.AccountAdministration;
using WebAppMVC.ViewModels.Responses.AccountAdministration;

namespace WebAppMVC.Areas.Account.Controllers
{
    [Area("AccountAdministration")]
    public class AccountController: BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateAccountAccountAdministrationRequestViewModel createAccountAccountAdministrationRequestViewModel)
        {
            return await ExecuteAsync(async () =>
            {
                CreateAccountAccountAdministrationResponseViewModel result
                    = await _accountService.CreateAccount(createAccountAccountAdministrationRequestViewModel);

                return RedirectToAction("SignIn", "Account", new { area = "AccountAdministration" });
            },
            createAccountAccountAdministrationRequestViewModel);
        }

        [HttpGet]
        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SignIn(SignInAccountAccountAdministrationRequestViewModel signInAccountAccountAdministrationRequestViewModel)
        {
            return await ExecuteAsync(async () =>
            {
                SignInAccountAccountAdministrationResponseViewModel result
                    = await _accountService.SignIn(signInAccountAccountAdministrationRequestViewModel);

                return RedirectToAction("Index", "Account", new { area = "AccountAdministration" });
            },
            signInAccountAccountAdministrationRequestViewModel);
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return await ExecuteAsync(async () =>
            {
                GetAccountAccountAdministrationResponseViewModel getAccountAccountAdministrationResponseViewModel
                    = await _accountService.GetAccount();

                return View(getAccountAccountAdministrationResponseViewModel);
            });
        }


        [HttpGet]
        public Task<IActionResult> SignOut()
        {
            return ExecuteAsync(() =>
            {
                _accountService.SignOut();
                return Task.FromResult<IActionResult>(RedirectToAction("SignIn", "Account", new { area = "AccountAdministration" }));
            });
        }

        [HttpPost]
        public async Task<IActionResult> Update(UpdateAccountAccountAdministrationRequestViewModel updateAccountAccountAdministrationRequestViewModel)
        {
            return await ExecuteAsync(async () =>
            {
                await _accountService.UpdateAccount(updateAccountAccountAdministrationRequestViewModel);

                return RedirectToAction("Index", "Account", new { area = "AccountAdministration" });
            },
            updateAccountAccountAdministrationRequestViewModel);
        }


        [HttpGet]
        public async Task<IActionResult> Delete()
        {
            return await ExecuteAsync(async () =>
            {
                await _accountService.DeleteAccount();

                return RedirectToAction("SignIn", "Account", new { area = "AccountAdministration" });
            });
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordAccountAccountAdministrationRequestViewModel changePasswordAccountAccountAdministrationRequestViewModel)
        {
            return await ExecuteAsync(async () =>
            {
                await _accountService.ChangePassword(changePasswordAccountAccountAdministrationRequestViewModel);

                return RedirectToAction("Index", "Account", new { area = "AccountAdministration" });
            },
            changePasswordAccountAccountAdministrationRequestViewModel);
        }
    }
}