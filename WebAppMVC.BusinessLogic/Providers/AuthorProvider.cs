﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WebAppMVC.BusinessLogic.Helpers.Interfaces;
using WebAppMVC.BusinessLogic.Providers.Interfaces;
using WebAppMVC.ViewModels.Responses;
using WebAppMVC.ViewModels.Responses.AuthorManagement;

namespace WebAppMVC.BusinessLogic.Providers
{
    public class AuthorProvider: ApiProvider, IAuthorProvider
    {
        public AuthorProvider(ICookieHelper cookieHelper, IConfiguration configuration) : base("author", cookieHelper, configuration)
        {
        }

        public async Task<GetAuthorAuthorManagementResponseViewModel> Get(long id)
        {
            GetAuthorAuthorManagementResponseViewModel getAuthorAuthorManagementResponseViewModel = await Get<GetAuthorAuthorManagementResponseViewModel>($"{id}");
            return getAuthorAuthorManagementResponseViewModel;
        }

        public async Task<GetAuthorListAuthorAuthorManagementResponseViewModel> GetAuthorList()
        {
            GetAuthorListAuthorAuthorManagementResponseViewModel getAuthorListAuthorAuthorManagementResponseViewModel = await Get<GetAuthorListAuthorAuthorManagementResponseViewModel>("get-author-list");
            return getAuthorListAuthorAuthorManagementResponseViewModel;
        }
    }
}
