﻿using System.Threading.Tasks;
using WebAppMVC.ViewModels.Responses;
using WebAppMVC.ViewModels.Responses.AuthorManagement;

namespace WebAppMVC.BusinessLogic.Providers.Interfaces
{
    public interface IAuthorProvider
    {
        Task<GetAuthorAuthorManagementResponseViewModel> Get(long id);
        Task<GetAuthorListAuthorAuthorManagementResponseViewModel> GetAuthorList();
    }
}
