﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WebAppMVC.BusinessLogic.Helpers;
using WebAppMVC.BusinessLogic.Helpers.Interfaces;
using WebAppMVC.Configs.AppSettings;

namespace WebAppMVC.BusinessLogic.Providers
{
    public class ApiProvider: HttpHelper
    {
        #region Variables
        private readonly string _controller;
        private readonly IConfiguration _configuration;
        #endregion

        #region Constructors
        public ApiProvider(string controller, ICookieHelper cookieHelper, IConfiguration configuration) : base(cookieHelper, configuration)
        {
            _configuration = configuration;
            _controller = $"api/{controller}";
        }
        #endregion

        #region Methods
        public async Task<T> Get<T>(string action)
        {
            T result = await GetBase<T>($"{ApiDomain}/{_controller}/{action}");
            return result;
        }

        public async Task<T> Post<T>(string action, object data)
        {
            T result = await PostBase<T>($"{ApiDomain}/{_controller}/{action}", data);
            return result;
        }

        public async Task Get(string action)
        {
            await GetBase($"{ApiDomain}/{_controller}/{action}");
        }

        public async Task Post(string action, object data)
        {
            await PostBase($"{ApiDomain}/{_controller}/{action}", data);
        }
        #endregion

        #region Properties
        private string _apiDomain;
        private string ApiDomain
        {
            get
            {
                if (string.IsNullOrEmpty(_apiDomain))
                {
                    IConfigurationSection domains = _configuration.GetSection(nameof(Domains));
                    _apiDomain = domains[nameof(Domains.ApiDomain)];
                }

                return _apiDomain;
            }
            set => _apiDomain = value;
        }
        #endregion Properties
    }
}
