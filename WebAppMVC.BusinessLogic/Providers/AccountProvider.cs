﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WebAppMVC.BusinessLogic.Helpers.Interfaces;
using WebAppMVC.BusinessLogic.Providers.Interfaces;
using WebAppMVC.ViewModels.Requests.AccountAdministration;
using WebAppMVC.ViewModels.Responses.AccountAdministration;

namespace WebAppMVC.BusinessLogic.Providers
{
    public class AccountProvider: ApiProvider, IAccountProvider
    {
        public AccountProvider(ICookieHelper cookieHelper, IConfiguration configuration) : base("account", cookieHelper, configuration)
        {
        }

        public async Task ChangePassword(
            ChangePasswordAccountAccountAdministrationRequestViewModel changePasswordAccountAccountAdministrationRequestViewModel)
        {
            await Post("change-password", changePasswordAccountAccountAdministrationRequestViewModel);
        }

        public async Task<CreateAccountAccountAdministrationResponseViewModel> CreateAccount(
            CreateAccountAccountAdministrationRequestViewModel createAccountAccountAdministrationRequestViewModel)
        {
            CreateAccountAccountAdministrationResponseViewModel createAccountAccountAdministrationResponseViewModel =
                await Post<CreateAccountAccountAdministrationResponseViewModel>("create", createAccountAccountAdministrationRequestViewModel);

            return createAccountAccountAdministrationResponseViewModel;
        }

        public async Task DeleteAccount()
        {
            await Get("delete");
        }

        public async Task<GetAccountAccountAdministrationResponseViewModel> GetAccount()
        {
            GetAccountAccountAdministrationResponseViewModel getAccountAccountAdministrationResponseViewModel =
                await Get<GetAccountAccountAdministrationResponseViewModel>(string.Empty);

            return getAccountAccountAdministrationResponseViewModel;
        }

        public async Task<RefreshTokenAccountAccountAdministrationResponseViewModel> RefreshToken(
            RefreshTokenAccountAccountAdministrationRequestViewModel refreshTokenAccountAccountAdministrationRequestViewModel)
        {
            RefreshTokenAccountAccountAdministrationResponseViewModel refreshTokenAccountAccountAdministrationResponseViewModel =
                await Post<RefreshTokenAccountAccountAdministrationResponseViewModel>("refresh-token", refreshTokenAccountAccountAdministrationRequestViewModel);

            return refreshTokenAccountAccountAdministrationResponseViewModel;
        }

        public async Task<SignInAccountAccountAdministrationResponseViewModel> SignIn(
            SignInAccountAccountAdministrationRequestViewModel signInAccountAccountAdministrationRequestViewModel)
        {
            SignInAccountAccountAdministrationResponseViewModel signInAccountAccountAdministrationResponseViewModel =
                await Post<SignInAccountAccountAdministrationResponseViewModel>("sign-in", signInAccountAccountAdministrationRequestViewModel);

            return signInAccountAccountAdministrationResponseViewModel;
        }

        public async Task UpdateAccount(
            UpdateAccountAccountAdministrationRequestViewModel updateAccountAccountAdministrationRequestViewModel)
        {
            await Post("update", updateAccountAccountAdministrationRequestViewModel);
        }
    }
}
