﻿using System;

namespace WebAppMVC.BusinessLogic.Exceptions
{
    public class BadRequestException: ApplicationException
    {
        public BadRequestException(string message) : base(message)
        {
        }
    }
}
