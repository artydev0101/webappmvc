﻿using System;

namespace WebAppMVC.BusinessLogic.Exceptions
{
    public class InternalServerErrorException: ApplicationException
    {
        public InternalServerErrorException(string message) : base(message)
        {
        }
    }
}
