﻿using System;

namespace WebAppMVC.BusinessLogic.Exceptions
{
    public class UnauthorizedException: ApplicationException
    {
        public UnauthorizedException(string message) : base(message)
        {
        }
    }
}
