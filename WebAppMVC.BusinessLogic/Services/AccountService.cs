﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebAppMVC.BusinessLogic.Helpers.Interfaces;
using WebAppMVC.BusinessLogic.Providers.Interfaces;
using WebAppMVC.BusinessLogic.Services.Interfaces;
using WebAppMVC.ViewModels.Requests.AccountAdministration;
using WebAppMVC.ViewModels.Responses.AccountAdministration;

namespace WebAppMVC.BusinessLogic.Services
{
    public class AccountService: IAccountService
    {
        private readonly ICookieHelper _cookieHelper;
        private readonly IAccountProvider _accountProvider;

        public AccountService(IAccountProvider accountProvider, ICookieHelper cookieHelper)
        {
            _cookieHelper = cookieHelper;
            _accountProvider = accountProvider;
        }

        public async Task ChangePassword(ChangePasswordAccountAccountAdministrationRequestViewModel changePasswordAccountAccountAdministrationRequestViewModel)
        {
            await _accountProvider.ChangePassword(changePasswordAccountAccountAdministrationRequestViewModel);
        }

        public async Task<CreateAccountAccountAdministrationResponseViewModel> CreateAccount(CreateAccountAccountAdministrationRequestViewModel createAccountAccountAdministrationRequestViewModel)
        {
            CreateAccountAccountAdministrationResponseViewModel createAccountAccountAdministrationResponseViewModel = await _accountProvider.CreateAccount(createAccountAccountAdministrationRequestViewModel);
            return createAccountAccountAdministrationResponseViewModel;
        }

        public async Task DeleteAccount()
        {
            await _accountProvider.DeleteAccount();
            ClearCookies();
        }

        public async Task<GetAccountAccountAdministrationResponseViewModel> GetAccount()
        {
            GetAccountAccountAdministrationResponseViewModel getAccountAccountAdministrationResponseViewModel = await _accountProvider.GetAccount();
            return getAccountAccountAdministrationResponseViewModel;
        }

        public async Task<RefreshTokenAccountAccountAdministrationResponseViewModel> RefreshToken(RefreshTokenAccountAccountAdministrationRequestViewModel refreshTokenAccountAccountAdministrationRequestViewModel)
        {
            RefreshTokenAccountAccountAdministrationResponseViewModel refreshTokenAccountAccountAdministrationResponseViewModel = await _accountProvider.RefreshToken(refreshTokenAccountAccountAdministrationRequestViewModel);
            return refreshTokenAccountAccountAdministrationResponseViewModel;
        }

        public async Task<SignInAccountAccountAdministrationResponseViewModel> SignIn(SignInAccountAccountAdministrationRequestViewModel signInAccountAccountAdministrationRequestViewModel)
        {
            SignInAccountAccountAdministrationResponseViewModel signInAccountAccountAdministrationResponseViewModel = await _accountProvider.SignIn(signInAccountAccountAdministrationRequestViewModel);

            _cookieHelper.Set(
               nameof(SignInAccountAccountAdministrationResponseViewModel.AccessToken),
               signInAccountAccountAdministrationResponseViewModel.AccessToken,
               new CookieOptions
               {
                   Expires = signInAccountAccountAdministrationResponseViewModel.AccessTokenExpirationDate
               },
               new CookieOptions
               {
               });

            _cookieHelper.Set(
                nameof(SignInAccountAccountAdministrationResponseViewModel.RefreshToken),
                signInAccountAccountAdministrationResponseViewModel.RefreshToken,
                new CookieOptions
                {
                    Expires = signInAccountAccountAdministrationResponseViewModel.RefreshTokenExpirationDate
                },
                new CookieOptions
                {
                });

            return signInAccountAccountAdministrationResponseViewModel;
        }

        public void SignOut()
        {
            ClearCookies();
        }

        private void ClearCookies()
        {
            _cookieHelper.Delete(nameof(SignInAccountAccountAdministrationResponseViewModel.AccessToken));
            _cookieHelper.Delete(nameof(SignInAccountAccountAdministrationResponseViewModel.RefreshToken));
        }

        public async Task UpdateAccount(UpdateAccountAccountAdministrationRequestViewModel updateAccountAccountAdministrationRequestViewModel)
        {
            await _accountProvider.UpdateAccount(updateAccountAccountAdministrationRequestViewModel);
        }
    }
}
