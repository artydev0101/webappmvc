﻿using System.Threading.Tasks;
using WebAppMVC.ViewModels.Requests.AccountAdministration;
using WebAppMVC.ViewModels.Responses.AccountAdministration;

namespace WebAppMVC.BusinessLogic.Services.Interfaces
{
    public interface IAccountService
    {
        Task<GetAccountAccountAdministrationResponseViewModel> GetAccount();
        Task<CreateAccountAccountAdministrationResponseViewModel> CreateAccount(CreateAccountAccountAdministrationRequestViewModel createAccountAccountAdministrationRequestViewModel);
        Task UpdateAccount(UpdateAccountAccountAdministrationRequestViewModel updateAccountAccountAdministrationRequestViewModel);
        Task DeleteAccount();
        Task<SignInAccountAccountAdministrationResponseViewModel> SignIn(SignInAccountAccountAdministrationRequestViewModel signInAccountAccountAdministrationRequestViewModel);
        Task<RefreshTokenAccountAccountAdministrationResponseViewModel> RefreshToken(RefreshTokenAccountAccountAdministrationRequestViewModel refreshTokenAccountAccountAdministrationRequestViewModel);
        Task ChangePassword(ChangePasswordAccountAccountAdministrationRequestViewModel changePasswordAccountAccountAdministrationRequestViewModel);
        void SignOut();
    }
}
