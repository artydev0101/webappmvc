﻿using Microsoft.AspNetCore.Http;
using WebAppMVC.BusinessLogic.Helpers.Interfaces;

namespace WebAppMVC.BusinessLogic.Helpers
{
    public class CookieHelper: ICookieHelper
    {
        private readonly HttpContext _httpContext;

        public CookieHelper(IHttpContextAccessor httpContextAccessor)
        {
            _httpContext = httpContextAccessor.HttpContext;
        }

        public string Get(string key)
        {
            string value = _httpContext.Request.Cookies[key];
            return value;
        }

        public void Set(string key, string value)
        {
            if (Contains(key))
            {
                Delete(key);
            }

            _httpContext.Response.Cookies.Append(key, value);
        }

        public void Set(string key, string value, CookieOptions appendOptions, CookieOptions deleteOptions)
        {
            if (Contains(key))
            {
                Delete(key, deleteOptions);
            }

            _httpContext.Response.Cookies.Append(key, value, appendOptions);
        }

        public void Delete(string key)
        {
            _httpContext.Response.Cookies.Delete(key);
        }

        public void Delete(string key, CookieOptions options)
        {
            _httpContext.Response.Cookies.Delete(key, options);

        }

        public bool Contains(string key)
        {
            bool result = _httpContext.Request.Cookies.ContainsKey(key);
            return result;
        }
    }
}
