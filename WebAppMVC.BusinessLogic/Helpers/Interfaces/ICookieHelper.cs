﻿using Microsoft.AspNetCore.Http;

namespace WebAppMVC.BusinessLogic.Helpers.Interfaces
{
    public interface ICookieHelper
    {
        string Get(string key);
        void Set(string key, string value);
        void Set(string key, string value, CookieOptions appendOptions, CookieOptions deleteOptions);
        void Delete(string key);
        void Delete(string key, CookieOptions options);
        bool Contains(string key);
    }
}
