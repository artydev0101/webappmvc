﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebAppMVC.BusinessLogic.Exceptions;
using WebAppMVC.BusinessLogic.Helpers.Interfaces;
using WebAppMVC.Configs.AppSettings;
using WebAppMVC.ViewModels.Requests.AccountAdministration;
using WebAppMVC.ViewModels.Responses.AccountAdministration;

namespace WebAppMVC.BusinessLogic.Helpers
{
    public class HttpHelper
    {
        #region Variables
        private const string _header = "application/json";
        private readonly ICookieHelper _cookieHelper;
        private readonly IConfiguration _configuration;
        #endregion Variables

        #region Constructors
        public HttpHelper(ICookieHelper cookieHelper, IConfiguration configuration)
        {
            _cookieHelper = cookieHelper;
            _configuration = configuration;
        }
        #endregion Constructors

        #region Methods
        private HttpClient CreateHttpClient()
        {
            var httpClient = new HttpClient
            {
                MaxResponseContentBufferSize = 2147483647,
                Timeout = TimeSpan.FromMinutes(1)
            };
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_header));

            return httpClient;
        }

        public async Task<T> GetBase<T>(string url)
        {
            var response = default(T);

            HttpResponseMessage responseMessage = await ExecuteGetAsync(url);

            response = await GetResult<T>(responseMessage);

            return response;
        }

        public async Task GetBase(string url)
        {
            HttpResponseMessage responseMessage = await ExecuteGetAsync(url);

            await ValidateResponse(responseMessage);
        }

        public async Task<T> PostBase<T>(string url, object data)
        {
            var response = default(T);

            HttpResponseMessage responseMessage = await ExecutePostAsync(url, data);

            response = await GetResult<T>(responseMessage);

            return response;
        }

        public async Task PostBase(string url, object data)
        {
            HttpResponseMessage responseMessage = await ExecutePostAsync(url, data);

            await ValidateResponse(responseMessage);
        }

        private async Task<HttpResponseMessage> ExecuteGetAsync(string url)
        {
            using (HttpClient client = CreateHttpClient())
            {
                string token = GetToken();

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                HttpResponseMessage responseMessage = await client.GetAsync(url);

                if (responseMessage.StatusCode == HttpStatusCode.Unauthorized)
                {
                    token = await RefreshAccessToken();

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    responseMessage = await client.GetAsync(url);
                }

                return responseMessage;
            }
        }

        private async Task<HttpResponseMessage> ExecutePostAsync(string url, object data)
        {
            using (HttpClient client = CreateHttpClient())
            {
                var response = default(HttpResponseMessage);

                string token = GetToken();

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                HttpContent content = null;

                if (data is List<KeyValuePair<string, string>> body)
                {
                    content = new FormUrlEncodedContent(body);
                }
                else
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_header));

                    var settings = new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver(),
                        NullValueHandling = NullValueHandling.Ignore
                    };

                    string jsonBody = JsonConvert.SerializeObject(data, Formatting.Indented, settings);

                    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(jsonBody);

                    content = new ByteArrayContent(buffer);

                    content.Headers.ContentType = new MediaTypeHeaderValue(_header);
                }

                response = await client.PostAsync(url, content);

                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    token = await RefreshAccessToken();

                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    response = await client.PostAsync(url, content);
                }

                return response;
            }
        }

        private async Task<T> GetResult<T>(HttpResponseMessage responseMessage)
        {
            var response = default(T);

            await ValidateResponse(responseMessage);

            if (responseMessage.StatusCode == HttpStatusCode.OK)
            {
                response = await DeserializeResponse<T>(responseMessage);
                return response;
            }

            return response;
        }

        private async Task ValidateResponse(HttpResponseMessage responseMessage)
        {
            if (responseMessage.StatusCode == HttpStatusCode.Unauthorized)
            {
                throw new UnauthorizedException("Token is not valid");
            }

            if (responseMessage.StatusCode == HttpStatusCode.BadRequest)
            {
                try
                {
                    string message = await responseMessage.Content.ReadAsStringAsync();

                    throw new BadRequestException(message);
                }
                catch (Exception ex) when (ex.GetType() != typeof(BadRequestException))
                {
                    throw new BadRequestException("Something Went Wrong");
                }
            }

            if (responseMessage.StatusCode == HttpStatusCode.InternalServerError)
            {
                throw new InternalServerErrorException("Internal Server Error");
            }
        }

        private async Task<T> DeserializeResponse<T>(HttpResponseMessage responseMessage)
        {
            var result = default(T);

            HttpContent content = responseMessage.Content;

            if (content == null)
            {
                throw new Exception("Could Not Deserialize Response. Content Was Null");
            }

            string contentJson = await content.ReadAsStringAsync();

            result = JsonConvert.DeserializeObject<T>(contentJson);

            return result;
        }
        #endregion Methods

        #region Token
        private string GetToken()
        {
            string accessToken = _cookieHelper.Get(nameof(SignInAccountAccountAdministrationResponseViewModel.AccessToken));
            return accessToken;
        }

        private async Task<string> RefreshAccessToken()
        {
            string refreshTokenCookieKey = nameof(SignInAccountAccountAdministrationResponseViewModel.RefreshToken);
            string accessTokenCookieKey = nameof(SignInAccountAccountAdministrationResponseViewModel.AccessToken);

            string refreshToken = _cookieHelper.Get(refreshTokenCookieKey);

            if (string.IsNullOrEmpty(refreshToken))
            {
                string accessToken = _cookieHelper.Get(accessTokenCookieKey);
                return accessToken;
            }

            IConfigurationSection endpoints = _configuration.GetSection(nameof(Endpoints));
            string _refreshTokenEndpoint = endpoints[nameof(Endpoints.RefreshTokenEndpoint)];

            var refreshTokenAccountAccountAdministrationRequestViewModel =
            new RefreshTokenAccountAccountAdministrationRequestViewModel
            {
                RefreshToken = refreshToken
            };

            RefreshTokenAccountAccountAdministrationResponseViewModel refreshTokenAccountAccountAdministrationResponseViewModel =
            await PostBase<RefreshTokenAccountAccountAdministrationResponseViewModel>(
                _refreshTokenEndpoint,
                refreshTokenAccountAccountAdministrationRequestViewModel);

            _cookieHelper.Set(accessTokenCookieKey, refreshTokenAccountAccountAdministrationResponseViewModel.AccessToken);
            _cookieHelper.Set(refreshTokenCookieKey, refreshTokenAccountAccountAdministrationResponseViewModel.RefreshToken);

            return refreshTokenAccountAccountAdministrationResponseViewModel.AccessToken;
        }
        #endregion Token
    }
}
