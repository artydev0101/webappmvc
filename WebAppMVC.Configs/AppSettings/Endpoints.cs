﻿namespace WebAppMVC.Configs.AppSettings
{
    public class Endpoints
    {
        public string RefreshTokenEndpoint { get; set; }
    }
}
