﻿using System.ComponentModel.DataAnnotations;

namespace WebAppMVC.ViewModels.Requests.AccountAdministration
{
    public class UpdateAccountAccountAdministrationRequestViewModel
    {
        /// <summary>
        /// First Name
        /// </summary>
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        /// <summary>
        /// Gender
        /// </summary>
        [Required]
        [Display(Name = "Gender")]
        public UpdateAccountAccountAdministrationRequestViewModelGenderType Gender { get; set; }
    }

    public enum UpdateAccountAccountAdministrationRequestViewModelGenderType
    {
        /// <summary>
        /// Undefined Gender
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Male Gender
        /// </summary>
        Male = 1,

        /// <summary>
        /// Female Gender
        /// </summary>
        Female = 2
    }
}
