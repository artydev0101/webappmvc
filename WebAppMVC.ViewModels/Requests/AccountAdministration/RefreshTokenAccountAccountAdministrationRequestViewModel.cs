﻿using System.ComponentModel.DataAnnotations;

namespace WebAppMVC.ViewModels.Requests.AccountAdministration
{
    public class RefreshTokenAccountAccountAdministrationRequestViewModel
    {
        /// <summary>
        /// Refresh Token
        /// </summary>
        [Required]
        [Display(Name = "Refresh Token")]
        public string RefreshToken { get; set; }
    }
}
