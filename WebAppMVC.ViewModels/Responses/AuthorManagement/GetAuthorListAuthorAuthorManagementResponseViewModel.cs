﻿using System.Collections.Generic;

namespace WebAppMVC.ViewModels.Responses.AuthorManagement
{
    public class GetAuthorListAuthorAuthorManagementResponseViewModel
    {
        /// <summary>
        /// Authors
        /// </summary>
        public IEnumerable<AuthorViewModelItem> AuthorList { get; set; }
    }

    public class AuthorViewModelItem
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }
    }
}
