﻿namespace WebAppMVC.ViewModels.Responses.AccountAdministration
{
    public class GetAccountAccountAdministrationResponseViewModel
    {
        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gender
        /// </summary>
        public GetAccountAccountAdministrationResponseViewModelGenderType Gender { get; set; }
    }

    public enum GetAccountAccountAdministrationResponseViewModelGenderType
    {
        /// <summary>
        /// Undefined Gender
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// Male Gender
        /// </summary>
        Male = 1,

        /// <summary>
        /// Female Gender
        /// </summary>
        Female = 2
    }
}
