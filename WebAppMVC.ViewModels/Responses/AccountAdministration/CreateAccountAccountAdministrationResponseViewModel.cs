﻿namespace WebAppMVC.ViewModels.Responses.AccountAdministration
{
    public class CreateAccountAccountAdministrationResponseViewModel
    {
        /// <summary>
        /// Account Id
        /// </summary>
        public long AccountId { get; set; }
    }
}
